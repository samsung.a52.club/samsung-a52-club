Samsung A52 club  
Кодовые имена: SM-A525F  
Кодовое имя на сайте Samsung: SM-A525F/DS

* 4pda
  * A52 https://4pda.ru/forum/index.php?showtopic=1000718
  * A51 https://4pda.ru/forum/index.php?showtopic=979790
  * A71 https://4pda.ru/forum/index.php?showtopic=975726
  * A72 https://4pda.ru/forum/index.php?showtopic=1000718
* xda
  * A52 https://forum.xda-developers.com/f/samsung-galaxy-a52-4g.12131/
* Обзоры, анонсы, характеристики
  * https://www.xda-developers.com/tag/samsung-galaxy-a52-4g/
  * https://www.samsung.com/ru/smartphones/galaxy-a/galaxy-a52-awesome-black-128gb-sm-a525fzkdser/
  * https://www.samsung.com/nz/smartphones/galaxy-a/galaxy-a52-awesome-black-128gb-sm-a525fzkhxnz/
  * Уровень SAR для DS (Dual Sim) https://www.samsung.com/sar/sarMain?site_cd=nz&prd_mdl_name=SM-A525F/DS&selNatCd=NZ&languageCode=EN
  * Уровень SAR https://www.samsung.com/sar/sarMain?site_cd=nz&prd_mdl_name=SM-A525F&selNatCd=NZ&languageCode=EN
* онлайн версия Galaxy Store https://galaxystore.samsung.com/
* Инструменты
  * https://github.com/david-lev/SamsungApkDownloader
    * Note. Ссылки на сервер samsungapps.com можно найти на сайте https://galaxystore.samsung.com/ и в документации 

# Удаление мусора

**Источники:**
* [1](https://technastic.com/remove-samsung-bloatware-safe-to-remove-apps/)
* [2](https://r1.community.samsung.com/t5/others/how-to-remove-samsung-bloatware-without-root/td-p/5817510)
* [3](https://www.thecustomdroid.com/galaxy-s20-bloatware-removal-guide/)
* [4](https://gist.github.com/patrickdappollonio/d86b47f2edb1e4a9be8696e0eb466a60)
* [5](https://github.com/AlexQuiniou/remove-bloatware-galaxy-s10e)
* [6](http://ru.c.mi.com/thread-1861377-1-1.html)

Список:
* AR
    ```
    areamoji
    ardrawing
    avatarsti
    ```
* ipsgeofence (не удалять) [1](https://www.cio.com/article/2383123/geofencing-explained.html), [2](https://www.samsung.com/us/support/answer/ANS00080662/)

# Особенности
* Без интернета новый телефон настроить нельзя, нужено 1 подключение или sim карта для определение региона, чтобы установились рекламные программы. Чистый Android - можно, но не в Samsung.
* Все возможные способы обойти FRP скорее всего не сработают, т.к. при запуске нет ни одной ссылки которая могла бы запустить chrome

# Скрытые настройки
## Good look
[Good Lock](https://galaxystore.samsung.com/detail/com.samsung.android.goodlock)  

https://vas.samsungapps.com/jupiter/stub/stubDownload.as?appId=com.samsung.android.goodlock&deviceId=SM-A525F&mcc=313&mnc=01&csc=ILO&sdkVer=26&pd=0&systemId=1608665720954&callerId=com.sec.android.app.samsungapps&abiType=64&extuk=0191d6627f38685f

https://vas.samsungapps.com/jupiter/stub/stubDownload.as?appId=com.samsung.android.goodlock&deviceId=SM-A525F&mcc=313&mnc=01&csc=ILO&sdkVer=26&pd=0&systemId=1608665720954&callerId=com.sec.android.app.samsungapps&abiType=64&extuk=0191d6627f38685f

При первом запуске телефона если много раз тапнуть на пустом месте где выбор языка появится сканер QR кода, видимо для подключения к WI FI или интеграции с чем-то

# Ускоряем копирование файлов
```sh
adb push c:\my_archive /sdcard/my_files # на телефон
adb pull /sdcard/DCIM c:\0_androidTmp   # с телефона
```

# Где хранится фото и видео
* `/storage/emulated/0/DCIM`
  * `/storage/emulated/0/DCIM/Camera`
  * `/storage/emulated/0/DCIM/Screenshots`
  * `/storage/emulated/0/DCIM/OpenCamera`
  * `/storage/emulated/0/DCIM/Videocaptures`
* `/storage/emulated/0/Pictures`
* `/storage/emulated/0/Movies`
* `/storage/emulated/0/Recordings`